import gitlab

token = "TOKEN"
group_id = "GUID"

gl = gitlab.Gitlab('https://gitlab.com', private_token=token)
group = gl.groups.get(group_id)
all_projects = group.projects.list(include_subgroups=True, all=True)

for each in all_projects:
    print(each.attributes['name_with_namespace'])

