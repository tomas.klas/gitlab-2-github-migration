import csv
import docker
import sys

from pip._internal.vcs import git


def process_line(docker_client, gitlab_image, github_repo, github_tag):
    # try to pull the image
    try:
        print("Pulling the image...")
        docker_client.images.pull(gitlab_image)
        print("Pulling DONE!!")
    except:
        print("Pulling FAILED!!")
        print("Try to login to registry.gitlab.com")
        return False

    try:
        print("Build a new dockerfile...")
        dockerfile = open("Dockerfile", "r+")
        dockerfile.write("FROM " + gitlab_image + "\n" +
                     "LABEL org.opencontainers.image.source=" + github_repo)
        docker_client.images.build(path="./", tag=github_tag)
        print("Build DONE!!")
    except:
        print("Building FAILED!!")
        print("Check the csv file for some issues..")
        return False

    try:
        print("Pushing image...")
        docker_client.images.push(github_tag, tag=None)
        print("Pushing DONE!!")
        return True
    except:
        print("Pushing FAILED!!")
        print("Try to login to ghcr.")


def main():
    # read file from command line

    docker_client = docker.from_env()
    with open(sys.argv[1], 'r') as file:
        line_count = 0
        reader = csv.reader(file)
        for row in reader:
            line_count += 1
            # fill arrays with new line variables
            log = """
------------------------------------------------------------
GITLAB_IMAGE=%s
GITHUB_IMAGE=%s
GITHUB_TAG=%s
            """ % (row[0], row[1], row[2])
            print(log)

            if not process_line(docker_client, row[0], row[1], row[2]):
                print("Falied line: ", line_count)
            else:
                print("DONE!!!")


if __name__ == "__main__":
    main()
