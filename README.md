# Gitlab 2 Github Migration

We are going to move our gitlab repositories to github
and this should help you with migrating the images to 
github repositories.

## How to migrate

Clone this repository, update the testCSV file. 
Then install dependacies and fire up the script.
Don't forget to login do gitlab registry and create a 
PAT token on github. 

### How to login to registry

Login with docker to Datamole Gitlab registry: 
`docker login registry.gitlab.com`. For username use 
your Datamole email and for password use your email password. 

### How to create PAT

Check this for more information: [link](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token).
